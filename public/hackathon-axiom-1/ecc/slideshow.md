class: dark, middle, center

## Elliptic-curve cryptography



_28 mars 2021_  
_Visios 2021_  
_Librelois <c@elo.tf>_

Suivre la présentation sur votre écran :

`librelois.duniter.io/slides/hackathon-axiom-1/ecc`

---

layout: true
class: dark

.center[Duniter GVA]

---

## Sommaire

1. [Disclaimer](#disclaimer)
1. [Arithmétique modulaire](#am)
1. [Corps commutatif](#corps)
1. [Elliptic-curve cryptography](#ecc)
1. [Ed25519](#ed25519)
1. [Nombres cryptographiques](#cryptint)
1. [Signature Ed25519](#sig)
1. [Transaction](#tx)
1. [Pedersen Commitment](#pc)
1. [Chiffrement des montants](#cm)
1. [Bulletproof](#bp)

---

name: disclaimer

Cete présentation est juste un aperçu pour comprendre dans les grandes lignes comment ça marche et pourquoi ça marche.
De nombreux détails d'implémentation critiques et subtils ne sont pas explicités.
Il ne faut jamais implémenter vous même les calculs de cryptographie sur courbe elliptique, vous devez utiliser des lib écrite où auditées par des mathématiciens cryptologues.

---

name: am

## .center[Arithmétique modulaire]

modulo: reste obtenu par une division euclidienne. 

Tout les calculs arithmétique se font modulo `n`.

Si a <= n alors  -a (mod n) = n - a (mod n)

Exemples:

3 + 6 (mod 5) = 4 (mod 5)

-2 (mod 5) = 3 (mod 5)

---

name: am2

## .center[Arithmétique modulaire]

Pour éviter l'overflow, on ne calcule pas directement `c = a + b (mod n)`.

On calcule `x = n - a`. Si `x > b` alors `c = a + b` sinon `c = b - x`.

Exemple: calculer 29 + 87 (mod 99)

x = 99 - 29 = 70

x <= b donc c = b - x  = 87 - 70 = 17 (mod 99)

---

name: corps

## .center[Corps commutatif]

Ensemble muni de deux opérations primitives (à deux opérandes) permettant de définir les additions, soustractions, multiplications et divisions.

* ℚ
* ℝ
* ℤ/nℤ est un corps commutatif si et seulement si n est un nombre premier (noté ℤ/pℤ ou 𝔽p)

---

name: ecc

## .center[Elliptic-curve cryptography]


Une courbe elliptique est définie par une paire (a, d) et l'ensemble des points de coordonnées (x,y) qui satisfont l'équation:

`a * x^2 +y^2 = 1 + dx^2 * y^2` avec `a,d,x,y ∈ 𝔽q`

--

L'ensemble des points (x,y) constitut alors un corps commutatif. Étant donné deux points P1 = (x1, y1) et P2 = (x2, y2), l'addition est définie comme suit :

![ecc addition](../../images/ecc_addition.png)

--

Ce type de corps à une propriétés particulière qui nous intéresse: la division est très difficile à caculer mais la multiplication est très facile à calculer.


---

name: ed25519

## .center[Ed25519]


Ed25519 est une courbe élliptique avec les coefficients (a= -1, d = − 121665/121666) :

`−x^2 + y^2 = 1 − 121665/121666 x 2y^2`

Avec `x, y ∈ 𝔽q`

Et avec q = `2^255 -19` (d'ou le nom).

Un trousseau est généré via un point particulier G :

Clé privée: un nombre entier `k`

Clé publique: `K = kG`

Chaque point du corps Ed25519 peut être représenté sous une forme compréssée de 32 octets.

---

name: cryptint

## .center[Nombres cryptographiques]

En informatique, un nombre entier est généralement encodé sur 32 bit où 64 bit.

Un nombre dit «cryptographique» est un nombre gargantuesquement grand.

Dans le cas des courbes élliptiques, les nombres entiers sont encodé sur 256 bit (32 octets).

---

name: sig

## .center[Signature Ed25519]

Objectif: on veut prouver que l'on connait `k` sans dévoiler sa valeur, en on veut «lier» cette preuve à un message `m`.

Génération de la preuve :

1. Le prouveur génère un nonce `α = H(H(k) || m)`
2. Le prouveur génère le challenge `c = H(αG || K || m)`
3. Le prouveur calcule la réponse `r = α + ck`
4. La signature est le couple `(αG, r)` (64 octets)

--

Vérif de la signature, le vérifieur connait: `K`, `m`, `αG` et `r` :

1. Le vérifieur génère le même challenge `c = H(αG || K || m)`
2. Le vérifieur calcule le point `rG` 
3. Le vérifieur calcule le point `αG + cK`
4. Si ces deux points sont égaux, la signature est valide.

--

Démonstration: `rG = (α + ck)G = αG + ckG = αG + cK`

---

name: tx

## .center[Transaction]

| Inputs | | Outputs |
|:-|-|-:|
| `1002:0:D:Do6...XqC:104937` | - | `2000:0:SIG(DTgQ...r9NG)` |
| `1002:0:D:Do6...XqC:104937` | - | `4:0:SIG(Do6Y...1XqC)` |

Les vérificateurs doivent pouvoir s'assurer que personne ne puisse dépenser de la monnaie qu'il n'a pas.

Pou cela quel sont les 2 critères ?

--
1. L'émétteur doit avoir le droit de dépenser la source

--

1. La somme des outputs doit être égale à la somme des inputs.

---

name: pc

## .center[Pedersen Commitment]

Une notion abtraite désignant queque chose dans lequel on peut « commiter » une valeur avec pour propriétés:

- Indicernabilité de la valeur
    - la connaissance de `C(x)` ne doit fournir aucune information sur `x`.
- Conservation de la somme
    - si `a + b = c` alors il faut que `C(a) + C(b) = C(c)`.

Pour avoir un Pedersen Commitment en ECC on utilise deux points G et H ainsi qu'un facteur d'aveuglement b:

.center[`C(a) = aG + bH`]

---

name: cm2

## .center[Chiffrement des montants]

Tout vérifieur doit pouvoir vérifier que la somme des outputs est égale à la somme des inputs.

| Inputs | | Outputs |
|:-|-|-:|
| `C1 = a1 * G + b1 * H` | ......... | `C3 = a3 * G + b3 * H` |
| `C2 = a2 * G + b2 * H` | ......... | `C4 = a4 * G + b4 * H` |


- Il suffit à l'émeteur de la transaction de choisir `b4 = b1 + b2 - b3`

=> tout les facteurs d'aveuglement sont générés aléatoirement sauf le dernier.

Le vérifieur à juste à vérifier que `C1 + C2 = C3 + C4`

--

.center[**En réalité c'est insuffisant, voyez vous pourquoi ?**]

---

name: bp

## .center[Bulletproof]

Une « range proof » permet de prouver que la valeur commitée est incluse dans un certain interval sans la dévoilée. 

Une bullet proof est un aggégat de plusieurs «range proof» permettant de prouver qu'une liste e valeurs commitées sont incluses dans l'intervale `[0; 2^n[`.

S'il y a P valeurs à prouver, la taille de la bulletproof est:

.center[`(2 * log(⁡n) + 9) * 32 * P`]

Si l'on choisi n=32, alors pour une transaction à 2 outputs on aura une bulletproof de 1216 octets.

Pour plus d'infos: [Bulletproof whitepaper](https://eprint.iacr.org/2017/1066.pdf)

---

## .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
