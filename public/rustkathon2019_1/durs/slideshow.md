class: dark, middle, center

# DURS (DUniter-RuSt)

![durs logo](../../images/Durs.png)

_23 mars 2019_  
_Rustkathon #1_  
_Librelois <elois@ifee.fr>_

Suivre la présentation sur votre ecran :

`librelois.duniter.io/slides/rustkathon2019_1/durs`

---

layout: true
class: dark

.center[DURS (DUniter-RuSt)]

---

## Sommaire

1. [Pourquoi Durs](#why)
2. [Historique du projet](#history)
3. [Durs actuellement](#currently)
4. [Etat fonctionnel](#functional-report)
5. [Depuis les rml12](#since-rml12)
6. [Roadmap d'elois pour les rml13](#elois-roadmap-rml13)
7. [A faire en priorité](#todo-priority)
8. [Architecture du projet](#archi)
9. [Le système de modules](#module-system)
10. [La pratique](#the-practice)

---

name: why

## .center[Pourquoi Durs]

![durs logo](../../images/Durs.64.png)

--

- Pour le plaisir de le faire
--

- Sécurité par la diversité (pas les mêmes failles)
--

- Haute dispo (relaye si bug critique sur l'autre implé)
--

- Oblige des spec DUP plus détaillés (dc moins de bug)
--

- 2 points de vues réduit drastiquement les oublis
--

- 2 fois plus de contributeurs touchables
--

- Gains de Rust :
  - Passage a l'échelle (Rust ultra performant & scalable)
  - Fiabilité (memory leaks, data races)
  - Ultra-light (bien pour les nano-pc, raspi zéro, etc)
--

- Réduire le bus factor :
  - Projet pensé collectif dés le départ
  - Priorise l'évolution horizontale plutot que verticale

???

Projet très long-terme
On prend le temps de faire les choses bien et de former le maximum de monde.

---

name: history

## .center[Historique du projet]

- Novembre 2017 Création du projet par Nanocryk.
  - ~ 3000 lignes. Crates crypto, wot, documents.
- Février 2018: Rejoins par elois en
- Mai 2018: Nanocryk quitte le projet pour prioriser `Fygg`.
- Fin mai 2018: [1ère prés. officielle de duniter-rs aux rml11](https://www.youtube.com/watch?v=fJvMRv1l5wM)
- Juillet 2018: v0.1.0-a1
- Septembre 2018 : Renommage Duniter-Rs -> Durs (demande cgeek)
- Octobre 2018 : Nouverua contributeur: inso.
- Novembre 2018: [2ème prés. aux rml12](https://www.youtube.com/watch?v=EIjJNTeaP2g)
- Janvier 2019: Nouveau contributeur counter
- Mars 2019 : 1er hackathon dédié au projet.

---

name: currently

## .center[Durs actuellement]

- 22_345 lignes de code
  - nanocryk ~ 4_000
  - elois ~ 18_000
  - inso ~ 500
  - counter ~ 5
- 22 crates (21 lib + 1 bin)
- ~500h de travail sur 13 mois

---

name: functional-report

## .center[Etat fonctionnel]

- Synchronisation rapide depuis une blockchain JSON en local
- Un noeud miroir qui reste synchronisé sur le réseau
- une base de donnée indexant la blockchain
- WS2P Privé
- une interface graphique dans le terminal
- Un explorateur de BDD fournissant :
  - date d'expiration des membres
  - règle de distance des membres
  - solde d'un compte
  - liste des certifications d'un membre

---

name: since-rml12

## .center[Depuis les rml12]

--

- Migration du code Rust en edition 2018
--

- Migration protocole DUP v10->v11 (rejeu des certif)
--

- Synchro depuis une blockchain JSON (pour compatibilité avec duniter 1.7)
--

- Implémentation de l'Algo de résolution des fork (inclus refonte du stockage des fork)
--

- Amélioration du build des paquets debian
--

- Création requete DAL `get_identities`
--

- Fix bug #129 (WS2Pv1 panic at the negotiation)

--

=> ~ 120h de travail sur 4 mois.

---

name: elois-roadmap-rml13

## .center[Roadmap d'elois pour les rml13]

- Tests d'intégration de la synchro rapide (export des index)
- Requetes DAL pour GVA
- Formation des nouveaux contributeurs
- Maintenance du code de gestion modulaire (en continue)

---

name: todo-priority

## .center[A faire en priorité]

- Implémentation de WS2Pv2

- Module GVA : nouvelle API Client GraphQL (counter et ji_emme)

- Module MemPool : sandbox des documents

- Sync cautious, implémenter toutes les règles du protocole DUP.

- Documenter au maximum l'existant

- Traduire les documentations

--

=> Formation et transmission de connaissances via talk mumble.

---

name: archi

## .center[architecture  du dépôt]

- .github : messages pour le dépot mirroir
- .gitlab : scripts python de publication des releases
- bin/ : crates binaires (durs-server, durs-desktop)
- doc/ : documentation haut niveau (pour dev ET utilisateurs)
- images/ : images statiques
- lib/ : crates bibliothèques (tools, core, modules)
  - core/ : crates constituant le squelette du programme (conf, boot, système modulaire)
  - modules/ : crates des modules
  - tools : Bibliothèques externalisables (code non lié a l'architecture de durs)
- releases/ : scripts bach de build des paquets
- target/ : contient les binaires générés (ignoré par git)

---

name: module-system

## .center[Le système de modules (1/5)]

![durs-modules-step-0](../../images/durs-modules-step-0.png)

- Chaque module est cloisonné dans un thread dédié.
- Ils interagissent uniquement par channels rust.

---

## .center[Le système de modules (2/5)]

![durs-modules-step-0](../../images/durs-modules-step-1.png)

- Le coeur lance les modules : ModName::start(..., ...)
- start() prend en param le RooterSender (+conf, etc)

---

## .center[Le système de modules (3/5)]

![durs-modules-step-0](../../images/durs-modules-step-2.png)

-> Chaque module s'enregistre auprès du router en transmettant une struct `ModuleRegistration`.

---

## .center[Le système de modules (4/5)]

![durs-modules-step-0](../../images/durs-modules-step-3.png)

- Puis les modules transmettent leurs messages au router.
- 3 types de messages : Event, Request, Response.

---

## .center[Le système de modules (5/5)]

![durs-modules-step-0](../../images/durs-modules-step-4.png)

- Le router filtre et relaie au(x) module(s) concernés.
- Destinataires ciblés via types de rôles et events.

---

name: the-practice

### La pratique

- Installez votre environnement de dev via [le tuto officiel](https://git.duniter.org/nodes/rust/duniter-rs/blob/dev/doc/fr/installer-son-environnement-de-dev.md) régulièrement mis a jours ([english version](https://git.duniter.org/nodes/rust/duniter-rs/blob/dev/doc/en/setup-your-dev-environment.md)).
- Développez votre module : atelier juste après.
- Chez vous en suivant [le tuto sur les modules](https://git.duniter.org/nodes/rust/duniter-rs/blob/dev/doc/fr/developper-un-module-durs.md) en cours de rédaction.

---

## .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]