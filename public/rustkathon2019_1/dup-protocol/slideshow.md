class: dark, middle, center

# Le protocole DUP

DUniter Protocol  
Dividende Universel Protocol

![duniter logo](../../images/Duniter.png)

_23 mars 2019_  
_Rustkathon #1_  
_Librelois <elois@ifee.fr>_

Suivre la présentation sur votre écran :

`librelois.duniter.io/slides/rustkathon2019_1/dup-protocol`

---

layout: true
class: dark

.center[Le protocole DUP]

---

## Sommaire

1. [DUBP et DUNP](#dubp-and-dunp)
2. [Etat vs transformation](#state-vs-transformation)
3. [Les Documents Utilisateur](#user-document)
4. [Réception d'un document](#document-receipt)
5. [Synchronisation](#sync)
6. [Génération d'un block](#block-generate)
7. [Réception d'un block](#block-receipt)
8. [Validation Locale vs Globale](#local-vs-global)
9. [Les indexs](#indexs)
10. [les évenements](#events)
11. [Résolution des Fork](#resolution-fork)
12. [Le Protocole réseau](#dunp)

---

name: dubp-and-dunp

# .center[DUBP et DUNP]

Historiquement DUP désignait uniquement le protocole **blockchain**

-> Or une crypto-monnaie **ne peut pas** fonctionner sans protocole réseau.

DUBP : Blockchain Protocol (v11) : [spécifications](https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/Protocol.md)

DUNP : Network Protocol (v1) : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md)

---

name: state-vs-transformation

# .center[Etat vs transformation]

La blockchain ne comporte que des transformations d'état

<div class="mermaid">
graph LR
    bloc-genesis-->E1..En
    E1..En-->E'1..E'n
    etat0-->etat1
    etat1-->etat2
</div>

Block = transformation = liste d'évenements.

Etat = Indexs -> persistés en base de donnée

Appliquer un bloc = Appliquer les transformations du bloc aux indexs de la monnaie.

=> Les noeuds Duniter/Durs/Juniter utilisent les indexs, pas la blockchain.

---

name: user-document

# .center[Les Documents Utilisateur]

Chaîne de caractère signée par la clé privée de l'utilisateur.

5 types de documents utilisateur :

- Identity
- Membership
- Certification
- Revocation
- Transaction

Exemple de chaque document sur [dup-tools-front](https://dup-tools-front.duniter.io/)

---

name: document-receipt

# .center[Réception d'un document (cas 1)]

<div class="mermaid">
sequenceDiagram
    participant clients
    clients->>local node: user document
    local node->>local node: Check document validity
    local node-->>clients: Ok, I saved your document well!
    local node->>local node: Add document to local sandox
    local node->>others nodes: Hi, please relay this document
</div>

---

name: general-cycle

# .center[Réception d'un document (cas 2)]

<div class="mermaid">
sequenceDiagram
    participant local node
    one other node->>local node: Hi, please relay this document
    local node->>local node: Check if already have
    local node->>local node: Check document validity
    local node->>local node: Add document to local sandox
    local node->>all others nodes: Hi, please relay this document
</div>

---

name: sync

# .center[Synchrnonisation]

<div class="mermaid">
sequenceDiagram
    local node->>others nodes: Give me the blockchain
    others nodes-->>local node: [blocks]
    loop blocks
        local node->>local node: Apply block
    end
</div>

---

name: block-generate

### .center[Génération d'un block]

<div class="mermaid">
sequenceDiagram
    local node->>local node: generate next block from sandbox
    loop PoW
        local node->>local node: increment nonce
    end
    alt find good hash
        local node->>others nodes: I'm find a valid block!
        local node-->>local node: send block to myself
    else too late
        others nodes-->>local node: new valid block!
    end
</div>

---

name: block-receipt

### .center[Réception d'un block]

<div class="mermaid">
sequenceDiagram
    local node->>local node: Local validation
    local node->>local node: Global validation (check all rules)
    alt valid
        local node->>local node: Apply block
        local node->>others nodes: relay valid block
    else invalid
        local node->>local node: tag blockstamp as invalid
    end
</div>

---

name: local-vs-global

## .center[Validation Locale vs Globale]

### -> Validation locale

- Vérifie cohérence d'un bloc bien formaté, sans autre contexte que le bloc lui-même

### -> Validation Globale

- Vérifie cohérence d'un bloc validé localement, dans le contexte de l'ensemble de la blockchain.
- S'effectue via l'application de plus de **105 règles** spécifiées [*ici*](https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/Protocol.md#br_g01-headnumber).
- C'est dans cette phase que les "indexs" entrent en jeux.

---

name: indexs

# .center[les indexs]

- IINDEX : index des identitités

    ```csv
    op, uid, pub, created_on, written_on, member, was_member, kick
    ```

- MINDEX : index des adhésions

    ```csv
    op, pub, created_on, written_on, expired_on, expires_on, revokes_on, chainable_on, type, revoked_on, leaving
    ```

- CINDEX : index des certifications

    ```csv
    op, issuer, receiver, created_on, written_on, sig, expires_on, chainable_on, replayable_on, expired_on
    ```

- SINDEX : index des sources de monnaie

    ```csv
    op, tx, identifier, pos, created_on, written_on, amount, base, conditions, consumed
    ```

---

name: events

# .center[les évenements]

2 types d'évenements :

- Événements utilisateurs : provoqués par un Document signé.
- Événements spontanés : provoqués par temps blockchain.

Les événements utilisateurs :

- **identities** : écriture d'une identité -> IINDEX + MINDEX
- **joiners** : entrée d'un membre -> IINDEX + MINDEX
- **actives** : renouvellement d'un membre -> MINDEX
- **leavers** : départ d'un membre -> MINDEX
- **revoked** : révocation d'un membre -> MINDEX
- **certifications** : écriture d'une certification -> CINDEX
- **transactions** : écriture d'une transaction -> SINDEX

Les événements spontanés :

- **excluded** : exclusion d'un membre -> IINDEX
- **certExpire** : expiration d'une certification -> CINDEX
- **implicitlyRevoked** : révocation d'un membre -> MINDEX

???

3ème cause possible d'un event : provoqué par un autre.
Par exemple l'event 'excluded' est provoqué par les event 'revoked' et 'implicitlyRevoked'.

---

name: resolution-fork

## .center[Résolution des Fork]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3-->A2
        B2-->A1
        C3-->A2
        C4-->C3
        C5-->C4
</div>

--
### .center[=> Le node A reste sur sa branche]

---

## .center[Résolution des Fork (2)]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3-->A2
        B2-->A1
        B3-->B2
        B4-->B3
        B5-->B4
        B6-->B5
        C3-->A2
        C4-->C3
        C5-->C4
</div>

--
#### .center[=> Le node A rollback 2 blocs puis empile la chaine B]

---

## .center[Résolution des Fork (3)]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3..A101-->A2
        A102-->A3..A101
        B2-->A1
        B3..B103-->B2
        B104-->B3..B103
        B105-->B104
        B106-->B105
        C3..C103-->A2
        C104-->C3..C103
        C105-->C104
</div>

--
### .center[=> Le node A reste sur sa branche]

---

name: dunp

# .center[Le protocole réseau]

- Basé sur WS2P (WebSocket To Peer) v1 : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md)
- connexions permanentes pour synchroniser :
  - les blocks
  - les documents utilisateurs
  - les sandbox des nodes
  - les fiches de peer
- 2 types de nodes : WS2P Public & WS2P Privé
  - WS2P public = avec endpoint
  - WS2P Privé = sans endpoint
- Les nodes avec WS2P Public ont aussi WS2P Privé d'activé.
- Chaque node a un quota de connexions limités (faible en entrée et élevé en sortie).

-> [Présentation de WS2Pv2](https://librelois.duniter.io/ws2pv2-rml12/)

---

# .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]