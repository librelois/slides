class: dark, middle, center

##Atelier TDD (Test Driven Development)

![durs logo](../../images/Durs.png)

_5 octobre 2019_  
_Rustkathon #2_  
_Librelois <c@elo.tf>_

Suivre la présentation sur votre écran :

`librelois.duniter.io/slides/rustkathon2019_2/tdd`

---

layout: true
class: dark

### .center[Atelier TDD]

---

## Sommaire

1. [Le principe](#principle)
2. [Installer les outils](#tools)
3. [Utilisation](#use)
4. [Atelier: objectifs et contraintes](#workshop-1)
5. [Atelier: comment démarrer](#workshop-2)

---

name: principle

A l'origine: écrire les tests avant de coder (Test First Design)

Désormais TDD (Test Driven Development) désigne tout une méthodologie, à base de micro-cycles :

1. Écrire un seul test qui décrit une partie du problème à résoudre.
2. Vérifier que le test échoue, autrement dit qu'il est valide, c'est-à-dire que le code se rapportant à ce test n'existe pas.
3. Écrire juste assez de code pour que le test réussisse.
4. Vérifier que le test passe, ainsi que les autres tests existants.
5. Puis remanier le code, c'est-à-dire l'améliorer sans en altérer le comportement.

Ce processus est répété en plusieurs cycles, jusqu'à résoudre le problème d'origine dans son intégralité.

---

name: tdd-cycle

![cycle TDD](../../images/cycle-tdd.png)

---

name: tools

Deux utilitaires cargo peuvent être utilisés:

1. cargo-watch

    ```bash
    cargo install cargo-watch
    ```

2. cargo-testify (notifie l'OS)

    ```bash
    apt-get install -y libdbus-1-dev
    cargo install cargo-testify
    ```

Dans les 2 cas, vous aurez besoin d'augmenter `max_user_watches`

```bash
sudo sysctl -w fs.inotify.max_user_watches=524288
```

---

name: use

Travailler sur la crate `crate-name` :

```bash
cargo watch -x 'test -p crate-name'
cargo testify -- -p crate-name
```

---

name: workshop-1

### .center[Atelier: objectifs et contraintes]

Objectifs :

- Obtenir la difficulté personnalisée d'un membre.
- Afficher la fenêtre courante et la difficulté personnalisée de chacun des membres qui la compose.

Ticket de suivi : #173

Description métier : [https://duniter.org/fr/wiki/duniter/preuve-de-travail/](https://duniter.org/fr/wiki/duniter/preuve-de-travail/)

Contraintes :

- L'obtention de la difficulté personnalisée d'un membre doit se faire en `O(1)`.
- Le store MAIN_BLOCKS ne doit pas être utilisé (cause #174)
- KISS (Keep It Stupid Simple)

---

name: workshop-2

### .center[Atelier: comment démarrer (1/2)]

Créez une nouvelle branche au nom de votre équipe ou de votre pseudo basée sur la branche `tp/personal-diffi` :

```bash
git checkout tp/personal-diffi
git checkout -b MY_TEAM_OR_PSEUDO/173-personal-diffi
```

Vérifiez que vous arrivez a compiler les tests et qu'ils échouent bien pour une erreur 'not yet implemented' :

```bash
cargo test -p durs-bc-db-writer
...
---- test_current_frame stdout ----
thread 'test_current_frame' panicked at 'not yet implemented', lib/modules-lib/bc-db-reader/src/current_frame.rs:45:5
```

---

name: workshop-3

### .center[Atelier: comment démarrer (2/2)]

- Implémentez les fonctions `unimplemented`, dans `lib/modules-lib/bc-db-reader/src/current_frame.rs`

- Vous pouvez créer de nouvelles entrées dans la collection CURRENT_META_DATAS

- Vous pouvez créer un/des nouveau(x) store(s) (suivez l'exemple de `store_name`). Pour chaque nouveau store créé il faut également :
  -  Définir son nom dans `bc-db-reader/src/constants.rs` (indiquer le type des clés et valeurs en commentaire).
  - l'ajouter dans le schéma de la BDD: `bc-db-reader/src/lib.rs`

---

## .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
