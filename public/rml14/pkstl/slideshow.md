class: dark, middle, center

## Public Key Secure Transport Layer

_28 novembre 2019_  
_RML #14_  
_Librelois <c@elo.tf>_

Suivre la présentation sur votre écran :

`librelois.duniter.io/slides/rml14/pkstl`

---

layout: true
class: dark

.center[PKSTL]

---

## Sommaire

- [Pourquoi ?](#why)
- [Comment ?](#how)
- [Diffie-Hellman](#dh)
- [Diffie-Hellman: peintures](#dh-peintures)
- [Implémentations](#impl)
- [Négociation](#nego)
- [Exemple de code](#code)

---

name: why

## .center[Pourquoi PKSTL ?]

- Besoin de départ :

  - Authentifier et chiffrer les communications entre nœuds Dunitrust.
  - De façon réellement décentralisée (pas dépendre d'une "autorité de certification").
  - Zéro config : doit pouvoir être activé par défaut et ne nécessiter aucune action spécifique de l'utilisateur de Dunitrust.

--

- Inconvénients sécurité par certificats :

  - Dépend d'une "autorité de certification", donc partiellement centralisé.
  - Nécessite de configurer un certificat (difficile, même pour un informaticien).
  - Nécessite de renouveler l'opération régulièrement ou de payer.

---

name: how

## .center[PKSTL: Comment ?]

- Chaque nœud Dunitrust à déjà une paire de clés Ed25519.

--

- Authentification assurée par la connaissance préalable de la clé publique du nœud contacté.

--

- Obtention d'un secret partagé par échange Diffie-Hellman de clés publiques Ed25519 éphémères signées.

--

- Dérivation du secret partager en credentials pour un algo de chiffrement symétrique.

--

=> RFC: https://git.duniter.org/nodes/common/doc/blob/ws2p_v2/rfc/0011_pkstl_v1.md

---

name: dh

## .center[Diffie-Hellman]

.center[![Diffie-Hellman](../../images/dh.png)]

---

name: dh-peintures

## .center[Diffie-Hellman: peintures]

.center[![Diffie-Hellman: peintures](../../images/dh-peintures.png)]

---

name: impl

## .center[Implémentations]

- En Rust : [crate pkstl du dépôt dunitrust](https://git.duniter.org/nodes/rust/duniter-rs/tree/dev/lib/tools/pkstl)

  - Algo. de chiffrement symétrique: Chacha20/Poly1305
  - Agnostique du protocole réseau (testé uniquement en websocket).
  - Inclus compression à la volée (deflate).
  - Coverage 84%

--

- Binding python: projet [py-pkstl](https://git.duniter.org/tools/py-pkstl) (tuxmain)

## .center[Utilisations]

- Déjà utilisé dans WS2Pv2

- A moyen terme: endpoint GVA pour clients lourds

- A long terme : plugin firefox ?

---

name: nego

## Négociation

<div class="mermaid">
sequenceDiagram
    participant C as Client
    participant S as Server
    C->>S: CONNECT
    S-->>C: CONNECT + ACK
    C->>S: ACK + DAC
    S-->>C: DAC
</div>

CONNECT: clé publique éphémère signée + clé de signature.  
ACK: Signature du hash sha256 de la clé publique éphémère de l'autre.  
*DAC : Données applicatives chiffrées.

---

name: code

## .center[Exemple de code]

```rust
use pkstl::*;

// Create secure layer
let sl_conf = SecureLayerConfig::default();
let sl = SecureLayer::create(sl_conf, None, Some(remote_pub))?;

let mut buffer = BufWriter::new(Vec::with_capacity(1_000));
sl.write_connect_msg_bin(None, &mut buffer)?;

match sl.read_bin(&incoming_msgs[..]) {
    Ok(msgs) => match {
        IncomingMessage::ConnectAndAck(my_ack) => {
            // Send my_ack and my app. datas
        }
    }
    Err(secure_err) => {
        // error handling..
    }
}
```

---

# .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de Duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
